package com.ruban.blackjack.model.response;

import com.ruban.blackjack.model.game.Card;
import com.ruban.blackjack.model.game.StatusGame;

import java.util.List;

public class GameResponse {
    private String gameId;
    private int userId;
    private double userWinSum;
    private List<Card> userCards;
    private List<Card> dealerCards;
    private StatusGame statusGameForUser;

    public GameResponse() {}

    public GameResponse(String gameId, int userId, double userWinSum, List<Card> userCards, List<Card> dealerCards, StatusGame statusGameForUser) {
        this.gameId = gameId;
        this.userId = userId;
        this.userWinSum = userWinSum;
        this.userCards = userCards;
        this.dealerCards = dealerCards;
        this.statusGameForUser = statusGameForUser;
    }

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public double getUserWinSum() {
        return userWinSum;
    }

    public void setUserWinSum(double userWinSum) {
        this.userWinSum = userWinSum;
    }

    public List<Card> getUserCards() {
        return userCards;
    }

    public void setUserCards(List<Card> userCards) {
        this.userCards = userCards;
    }

    public List<Card> getDealerCards() {
        return dealerCards;
    }

    public void setDealerCards(List<Card> dealerCards) {
        this.dealerCards = dealerCards;
    }

    public StatusGame getStatusGameForUser() {
        return statusGameForUser;
    }

    public void setStatusGameForUser(StatusGame statusGameForUser) {
        this.statusGameForUser = statusGameForUser;
    }

    @Override
    public String toString() {
        return "GameResponse{" +
                "gameId=" + gameId +
                ", userId=" + userId +
                ", userWinSum=" + userWinSum +
                ", userCards=" + userCards +
                ", dealerCards=" + dealerCards +
                ", statusGameForUser=" + statusGameForUser +
                '}';
    }
}
