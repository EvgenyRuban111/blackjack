package com.ruban.blackjack.model;


public class NewGameRequest {

    private int userId;
    private double userBet;

    public NewGameRequest() {}

    public NewGameRequest(int userId, double userBet) {
        this.userId = userId;
        this.userBet = userBet;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public double getUserBet() {
        return userBet;
    }

    public void setUserBet(double userBet) {
        this.userBet = userBet;
    }

    @Override
    public String toString() {
        return "NewGameRequest{" +
                "userId=" + userId +
                ", userBet=" + userBet +
                '}';
    }
}
