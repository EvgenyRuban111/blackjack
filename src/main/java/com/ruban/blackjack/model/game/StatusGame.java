package com.ruban.blackjack.model.game;

public enum StatusGame {
    WIN, PUSH, BUST, CONTINUE
}
