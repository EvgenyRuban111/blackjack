package com.ruban.blackjack.model.game;

public enum Winner {
    USER, DEALER, DRAW
}
