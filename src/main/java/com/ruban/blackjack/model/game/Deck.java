package com.ruban.blackjack.model.game;

import java.util.*;

public class Deck {

    private LinkedList<Card> cards;

    public Deck() {}

    public Deck(LinkedList<Card> cards) {
        this.cards = cards;
    }

    public LinkedList<Card> getCards() {
        return cards;
    }

    public void setCards(LinkedList<Card> cards) {
        this.cards = cards;
    }

    @Override
    public String toString() {
        return "Deck{" +
                "cards=" + cards +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Deck deck = (Deck) o;

        return cards != null ? cards.equals(deck.cards) : deck.cards == null;

    }

    @Override
    public int hashCode() {
        return cards != null ? cards.hashCode() : 0;
    }
}
