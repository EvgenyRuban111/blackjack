package com.ruban.blackjack.model.game;

public enum CardsOwner {
    USER_CARDS, DEALER_CARDS, BOTH_CARDS;

    @Override
    public String toString() {
        return "CardsOwner{}";
    }
}
