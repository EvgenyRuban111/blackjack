package com.ruban.blackjack.model.game;

public enum Suit {
    HEARTS, SPADES, CLUBS, DIAMONDS
}
