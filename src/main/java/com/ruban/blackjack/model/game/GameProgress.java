package com.ruban.blackjack.model.game;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.data.annotation.Id;

import javax.persistence.Column;
import java.util.LinkedList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class GameProgress {

    @Id
    private String gameId;

    @Column
    private int userId;
    @Column
    private double userBet;
    @Column
    private Deck deck;
    @Column
    private List<Card> userCards = new LinkedList<>();
    @Column
    private List<Card> dealerCards = new LinkedList<>();
    @Column
    private StatusGame statusGameForUser;

    public GameProgress() {}

    public String getGameId() {
        return gameId;
    }

    public void setGameId(String gameId) {
        this.gameId = gameId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public double getUserBet() {
        return userBet;
    }

    public void setUserBet(double userBet) {
        this.userBet = userBet;
    }

    public Deck getDeck() {
        return deck;
    }

    public void setDeck(Deck deck) {
        this.deck = deck;
    }

    public List<Card> getUserCards() {
        return userCards;
    }

    public void setUserCards(List<Card> userCards) {
        this.userCards = userCards;
    }

    public List<Card> getDealerCards() {
        return dealerCards;
    }

    public void setDealerCards(List<Card> dealerCards) {
        this.dealerCards = dealerCards;
    }

    public StatusGame getStatusGameForUser() {
        return statusGameForUser;
    }

    public void setStatusGameForUser(StatusGame statusGameForUser) {
        this.statusGameForUser = statusGameForUser;
    }

    @Override
    public String toString() {
        return "GameProgress{" +
                "gameId=" + gameId +
                ", userId=" + userId +
                ", userBet=" + userBet +
                ", deck=" + deck +
                ", userCards=" + userCards +
                ", dealerCards=" + dealerCards +
                ", statusGameForUser=" + statusGameForUser +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GameProgress that = (GameProgress) o;

        if (userId != that.userId) return false;
        if (Double.compare(that.userBet, userBet) != 0) return false;
        return statusGameForUser == that.statusGameForUser;

    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = userId;
        temp = Double.doubleToLongBits(userBet);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (statusGameForUser != null ? statusGameForUser.hashCode() : 0);
        return result;
    }
}
