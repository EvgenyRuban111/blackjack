package com.ruban.blackjack.model.game;

public enum UserStepInGame {
    FIRST, NEXT
}
