package com.ruban.blackjack.model.event;

public enum EventType {
    GAME_TYPE, BALANCE_TYPE
}
