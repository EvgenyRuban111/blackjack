package com.ruban.blackjack.model.event;


import javax.persistence.*;
import java.util.Date;


@Entity
public class Event {
    private int id;
    private int userId;
    private Date date = new Date();
    private String log;
    private EventType eventType;

    public Event() {
    }

    public Event(int userId, String log, EventType eventType) {
        this.userId = userId;
        this.log = log;
        this.eventType = eventType;
    }

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Temporal(TemporalType.TIMESTAMP)
    @Column
    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Column
    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

    @Enumerated(EnumType.STRING)
    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", userId=" + userId +
                ", date=" + date +
                ", log='" + log + '\'' +
                ", eventType=" + eventType +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Event event = (Event) o;

        if (userId != event.userId) return false;
        if (date != null ? !date.equals(event.date) : event.date != null) return false;
        if (log != null ? !log.equals(event.log) : event.log != null) return false;
        return eventType == event.eventType;

    }

    @Override
    public int hashCode() {
        int result = userId;
        result = 31 * result + (date != null ? date.hashCode() : 0);
        result = 31 * result + (log != null ? log.hashCode() : 0);
        result = 31 * result + (eventType != null ? eventType.hashCode() : 0);
        return result;
    }
}
