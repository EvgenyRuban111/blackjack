package com.ruban.blackjack.controller;


import com.ruban.blackjack.model.event.Event;
import com.ruban.blackjack.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
class EventController {


    private EventService eventService;

    @Autowired
    void setEventService(EventService eventService) {
        this.eventService = eventService;
    }

    @RequestMapping(
            value= "events/{eventId}" ,
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    ResponseEntity<Event> getEvent(@PathVariable("eventId") int eventId) {
        Event event = eventService.getEventById(eventId);
        if (event == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {
            return new ResponseEntity<>(event, HttpStatus.OK);
        }

    }

    @RequestMapping(
            value= "events/{eventId}" ,
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    ResponseEntity<String> deleteEvent(@PathVariable("eventId") int eventId) {
        eventService.deleteEventById(eventId);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }

    @RequestMapping(
            value= "events/user/{userId}" ,
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    ResponseEntity<Iterable<Event>> getAllEventsByUser(@PathVariable("userId") int userId) {
        Iterable<Event> events = eventService.getEventsByUserId(userId);
        return new ResponseEntity<>(events, HttpStatus.OK);
    }

    @RequestMapping(
            value= "events/user/{userId}" ,
            method = RequestMethod.DELETE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    ResponseEntity<String> deleteAllEventsByUser(@PathVariable("userId") int userId) {
        eventService.deleteEventsByUserId(userId);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }

}
