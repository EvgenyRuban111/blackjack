package com.ruban.blackjack.controller;


import com.ruban.blackjack.model.User;
import com.ruban.blackjack.exception.UserNotFoundException;
import com.ruban.blackjack.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
class UserController {

    private UserService userService;

    @Autowired
    void setUserService(UserService userService) {
        this.userService = userService;
    }

    @RequestMapping(
            value= "users/new" ,
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    ResponseEntity<Integer> createUser(@RequestBody User user) {
        if (user != null) {
            User saveUser =  userService.saveUser(user);
            return new ResponseEntity<>(saveUser.getId(), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
    }

    @RequestMapping(value= "users/{userId}" , method = RequestMethod.GET)
    @ResponseBody
    ResponseEntity<User> getUserById(@PathVariable("userId") int userId) {
        User user = userService.getUserById(userId);
        if (user == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @RequestMapping(value= "users/{userId}" , method = RequestMethod.DELETE)
    @ResponseBody
    ResponseEntity<String> deleteUserById(@PathVariable("userId") int userId) {
        userService.deleteUserById(userId);
        return new ResponseEntity<>("Success", HttpStatus.OK);
    }

    @RequestMapping(value= "users/{userId}/balance/{updateSum}" , method = RequestMethod.PUT)
    @ResponseBody
    ResponseEntity<String> updateUserBalance(@PathVariable("userId") int userId, @PathVariable("updateSum") double updateSum) {
        boolean isUpdated;
        try {
            isUpdated = userService.updateUserBalance(userId, updateSum);
        } catch (UserNotFoundException e) {
            return new ResponseEntity<>("Success", HttpStatus.NOT_FOUND);
        }
        if (isUpdated) {
            return new ResponseEntity<>("Success", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Failed", HttpStatus.NOT_MODIFIED);
        }
    }
}
