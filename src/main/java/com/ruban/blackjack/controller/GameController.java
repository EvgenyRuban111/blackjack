package com.ruban.blackjack.controller;

import com.ruban.blackjack.model.NewGameRequest;
import com.ruban.blackjack.model.response.GameResponse;
import com.ruban.blackjack.exception.NotEnoughMoneyException;
import com.ruban.blackjack.exception.GameNotFoundException;
import com.ruban.blackjack.exception.UserNotFoundException;
import com.ruban.blackjack.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
class GameController {


    private GameService gameService;

    @Autowired
    void setGameService(GameService gameService) {
        this.gameService = gameService;
    }

    @RequestMapping(
            value= "games/start" ,
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    ResponseEntity<GameResponse> startNewGame(@RequestBody NewGameRequest newGameRequest) {
        GameResponse response;
        try {
            response = gameService.startNewGame(newGameRequest.getUserId(), newGameRequest.getUserBet());
        } catch (NotEnoughMoneyException e) {
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        } catch (UserNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(
            value= "games/hit/{gameId}/user/{userId}" ,
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    ResponseEntity<GameResponse> hit(@PathVariable("gameId") String gameId, @PathVariable("userId") int userId) {
        GameResponse response;
        try {
            response = gameService.hit(gameId, userId);
        } catch (UserNotFoundException | GameNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(
            value= "games/stand/{gameId}/user/{userId}" ,
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    ResponseEntity<GameResponse> stand(@PathVariable("gameId") String gameId, @PathVariable("userId") int userId) {
        GameResponse response;
        try {
            response = gameService.stand(gameId, userId);
        } catch (UserNotFoundException | GameNotFoundException e) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(
            value= "games/leave/{gameId}/user/{userId}" ,
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    ResponseEntity<String> leaveGame(@PathVariable("gameId") String gameId, @PathVariable("userId") int userId) {
        ResponseEntity<String> response;
        boolean isDeleted = gameService.leaveGame(gameId, userId);
        if (isDeleted) {
            response = new ResponseEntity<>("Your leave game with id: " + gameId, HttpStatus.OK);
        } else {
            response = new ResponseEntity<>("Your didnt leave game with id: " + gameId, HttpStatus.OK);
        }

        return response;
    }
}
