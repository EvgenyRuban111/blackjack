package com.ruban.blackjack.exception;

public class NotEnoughMoneyException extends RuntimeException {

    private String msg;

    public NotEnoughMoneyException(String msg) {
        super(msg);
    }

    public String getMsg() { return msg; }

    public void setMsg(String msg) { this.msg = msg; }
}
