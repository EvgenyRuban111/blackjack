package com.ruban.blackjack.exception;

public class UserNotFoundException extends RuntimeException {

    private String msg;

    public UserNotFoundException(String msg) {
        super(msg);
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
