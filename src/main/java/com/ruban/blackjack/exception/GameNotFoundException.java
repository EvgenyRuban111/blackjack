package com.ruban.blackjack.exception;

public class GameNotFoundException extends RuntimeException {

    private String msg;

    public GameNotFoundException(String msg) {
        super(msg);
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
