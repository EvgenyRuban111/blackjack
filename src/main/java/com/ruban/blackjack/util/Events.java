package com.ruban.blackjack.util;

public class Events {

    private Events() {
    }

    public static String updateBalance(int userId, double sum) {
        return "User:" + userId + " updated balance on sum: " + sum;
    }

    public static String updateBalanceOnSum(int userId, double sum) {
        return "User:" + userId + " update balance on sum: " + sum;
    }

    public static String newGame(int userId, double betSum) {
        return "User:" + userId + " started new game with bet: " + betSum;
    }

    public static String hitMove(int userId, String gameId) {
      return "User:" + userId + " made hit in game: " + gameId;
    }

    public static String standMove(int userId, String gameId) {
        return "User:" + userId + " made stand in game: " + gameId;
    }

    public static String win(int userId, String gameId) {
        return "User:" + userId + " won on game: " + gameId;
    }

    public static String bust(int userId, String gameId) {
        return "User:" + userId + " bust on game: " + gameId;
    }

    public static String push(int userId, String gameId) {
        return "User:" + userId + " pushed on game: " + gameId;
    }

    public static String gameContinues(int userId, String gameId) {
        return "User:" + userId + " continues game: " + gameId;
    }

    public static String leaveGame(int userId, String gameId) {
        return "User:" + userId + " leaved game: " + gameId;
    }

}
