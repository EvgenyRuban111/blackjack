package com.ruban.blackjack.service;

import com.ruban.blackjack.model.response.GameResponse;
import com.ruban.blackjack.exception.NotEnoughMoneyException;
import com.ruban.blackjack.exception.GameNotFoundException;
import com.ruban.blackjack.exception.UserNotFoundException;

public interface GameService {

    GameResponse startNewGame(int userId, double userBet) throws NotEnoughMoneyException, UserNotFoundException;

    Boolean leaveGame(String gameId, int userId);

    GameResponse hit(String gameId, int userId) throws UserNotFoundException, GameNotFoundException;

    GameResponse stand(String gameId, int userId) throws UserNotFoundException, GameNotFoundException;

}
