package com.ruban.blackjack.service.implementation;

import com.ruban.blackjack.dao.EventDAO;
import com.ruban.blackjack.model.event.Event;
import com.ruban.blackjack.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@Transactional
class EventServiceImpl implements EventService {


    private EventDAO eventDAO;

    @Autowired
    void setEventDAO(EventDAO eventDAO) {
        this.eventDAO = eventDAO;
    }

    @Override
    public Event getEventById(Integer eventId) {
        return eventDAO.findOne(eventId);
    }

    @Override
    public void deleteEventById(Integer eventId) {
        eventDAO.delete(eventId);
    }

    @Override
    public Iterable<Event> getEventsByUserId(Integer userId) {
        return eventDAO.findAllByUserId(userId);
    }

    @Override
    public void deleteEventsByUserId(Integer userId) {
        eventDAO.deleteAllByUserId(userId);
    }
}
