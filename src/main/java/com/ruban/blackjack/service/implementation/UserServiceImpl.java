package com.ruban.blackjack.service.implementation;


import com.ruban.blackjack.dao.EventDAO;
import com.ruban.blackjack.model.User;
import com.ruban.blackjack.dao.UserDAO;
import com.ruban.blackjack.exception.UserNotFoundException;
import com.ruban.blackjack.model.event.Event;
import com.ruban.blackjack.model.event.EventType;
import com.ruban.blackjack.service.UserService;
import com.ruban.blackjack.util.Events;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Objects;

@Service
@Transactional
class UserServiceImpl implements UserService {

    private UserDAO userDAO;
    private EventDAO eventDAO;

    @Autowired
    void setEventDAO(EventDAO eventDAO) {
        this.eventDAO = eventDAO;
    }

    @Autowired
    void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    public User saveUser(User user) { return userDAO.save(user); }

    public User getUserById(Integer userId) { return userDAO.findOne(userId); }

    public void deleteUserById(Integer userId) { userDAO.delete(userId); }

    public boolean updateUserBalance(Integer userId, double updateSum) throws UserNotFoundException {
        User user = userDAO.findOne(userId);
        if (user != null) {
            Event updateBalance = new Event(userId, Events.updateBalanceOnSum(userId, updateSum), EventType.BALANCE_TYPE);
            eventDAO.save(updateBalance);
            double oldBalance = user.getBalance();
            user.setBalance(updateSum + oldBalance);
            User updatedUser = userDAO.save(user);
            return Objects.equals(updatedUser.getBalance(), oldBalance + updateSum);
        } else {
            throw new UserNotFoundException("User: " + userId + " not found");
        }
    }
}
