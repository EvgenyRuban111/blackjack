package com.ruban.blackjack.service.implementation;


import com.ruban.blackjack.dao.*;
import com.ruban.blackjack.model.User;
import com.ruban.blackjack.model.event.*;
import com.ruban.blackjack.service.GameService;
import com.ruban.blackjack.util.Events;
import com.ruban.blackjack.model.game.*;
import com.ruban.blackjack.model.response.GameResponse;
import com.ruban.blackjack.exception.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import static com.ruban.blackjack.model.game.StatusGame.*;

@Service
@Transactional
class GameServiceImpl implements GameService {

    private GameProgress game;
    private GameDAOMongo gameDAO;
    private UserDAO userDAO;
    private EventDAO eventDAO;

    private static final int BLACK_JACK = 21;
    private static final int END_DEALER_LOGIC_BOUNDARY = 17;

    @Autowired
    void setGameDAO(GameDAOMongo gameDAO) {
        this.gameDAO = gameDAO;
    }

    @Autowired
    void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Autowired
    void setEventDAO(EventDAO eventDAO) {
        this.eventDAO = eventDAO;
    }

    @Override
    public GameResponse startNewGame(int userId, double userBet) throws NotEnoughMoneyException, UserNotFoundException {
        User user = userDAO.findOne(userId);
        GameResponse response;
        if (user != null && isEnoughBalance(user, userBet)) {
            saveMoveEvents(new Event(userId, Events.newGame(userId, userBet), EventType.GAME_TYPE));
            saveUpdateBalanceEvents(user, userBet);
            response = initGame(userId, userBet);
            if (response.getStatusGameForUser() == WIN) {
                saveVictoriousEvents(user, response);
            } else if (response.getStatusGameForUser() == PUSH) {
                savePushEvents(user, response, userBet);
            } else {
                saveContinuesEvents(user);
            }
        } else {
            throw new UserNotFoundException("User not found: " + userId);
        }
        return response;
    }

    @Override
    public Boolean leaveGame(String gameId, int userId) {
        Event leaveGame = new Event(userId, Events.leaveGame(userId, gameId), EventType.GAME_TYPE);
        eventDAO.save(leaveGame);
        gameDAO.delete(gameId);
        return gameDAO.findOne(gameId) == null;
    }

    @Override
    public GameResponse hit(String gameId, int userId) throws UserNotFoundException, GameNotFoundException {
        User user = userDAO.findOne(userId);
        GameResponse response;
        if (user != null) {
            checkMovePermission(gameId, userId);
            saveMoveEvents(new Event(userId, Events.hitMove(userId, gameId), EventType.GAME_TYPE));
            response = hitMove(userId);
            if (response.getStatusGameForUser() == StatusGame.WIN) {
                saveVictoriousEvents(user, response);
            } else if (response.getStatusGameForUser() == StatusGame.BUST) {
                saveBustEvents(user, response);
            } else if (response.getStatusGameForUser() == StatusGame.PUSH) {
                savePushEvents(user, response, game.getUserBet());
            } else {
                saveContinuesEvents(user);
            }
        } else {
            throw new UserNotFoundException("User not found: " + userId);
        }
        return response;
    }

    @Override
    public GameResponse stand(String gameId, int userId) throws UserNotFoundException, GameNotFoundException {
        User user = userDAO.findOne(userId);
        GameResponse response;
        if (user != null) {
            checkMovePermission(gameId, userId);
            saveMoveEvents(new Event(userId, Events.standMove(userId, gameId), EventType.GAME_TYPE));
            response = standMove(userId);
            if (response.getStatusGameForUser() == StatusGame.WIN) {
                saveVictoriousEvents(user, response);
            } else if (response.getStatusGameForUser() == StatusGame.BUST) {
                saveBustEvents(user, response);
            } else {
                savePushEvents(user, response, game.getUserBet());
            }
        } else throw new UserNotFoundException("User not found: " + userId);
        return response;
    }

    /**
     * Method for check user balance before start new game
     *
     * @param currentUser current user
     * @param userBet user bet
     * @return enough or not money for start game
     * @throws NotEnoughMoneyException if not enough money for new game
     */
    private boolean isEnoughBalance(User currentUser, double userBet) throws NotEnoughMoneyException {
        if (currentUser.getBalance() >= userBet) {
            return true;
        } else {
            throw new NotEnoughMoneyException("User: " + currentUser + " has not enough money. User balance: " + currentUser.getBalance());
        }
    }

    /**
     * Method for init and shuffle new deck for current game
     *
     * @return new shuffle deck with 18 cards (18 is max numbers of cards needed for one user and dealer in blackjack game)
     */
    private Deck deckInitAndShuffle() {
        LinkedList<Card> cards = new LinkedList<>();
        for(Suit suit: Suit.values()) {
            for(Rank rank: Rank.values()) {
                cards.add(new Card(suit, rank));
            }
        }
        Collections.shuffle(cards);
        return new Deck(new LinkedList<>(cards.subList(0, 18)));
    }

    /**
     * Method for check winner by points
     *
     * @param userPoints sum of users points
     * @param dealerPoints sum of dealer points
     * @return winner
     */
    private Winner checkWinner(int userPoints, int dealerPoints) {
        if (userPoints > dealerPoints) {
            return Winner.USER;
        } else if (userPoints < dealerPoints) {
            return Winner.DEALER;
        } else {
            return Winner.DRAW;
        }
    }

    /**
     * Method for sum points
     *
     * @param cards cards on hands
     * @return sum of points for current cards on hands
     */
    private int sumPoints(List<Card> cards) {
        int sum = cards.stream().mapToInt(card -> card.getRank().getPoints()).sum();
        int aces = Collections.frequency(cards, new Card(null, Rank.ACE));
        while (sum > BLACK_JACK && aces != 0) {
            sum -= 10;
            aces--;
        }
        return sum;
    }

    /**
     * Method for make hit move for user
     *
     * @param userId current user
     * @return current game progress after hit move
     * @throws UserNotFoundException if user will not found
     * @throws GameNotFoundException if game will not found
     */
    private GameResponse hitMove(int userId) throws UserNotFoundException, GameNotFoundException {
        GameResponse gameResponse;
        dealCards(1, CardsOwner.USER_CARDS);
        int userPoints = sumPoints(game.getUserCards());
        if (userPoints == BLACK_JACK) {
            double userWinSum = dealerLogic();
            gameResponse = createResponse(userId, userWinSum, userPoints);
        }  else {
            if (userPoints > BLACK_JACK) {
                game.setStatusGameForUser(StatusGame.BUST);
            } else {
                game.setStatusGameForUser(StatusGame.CONTINUE);
            }
            gameResponse = createResponse(userId, 0.0, userPoints);
        }

        gameDAO.save(game);

        return gameResponse;
    }

    /**
     * Method for make hit move for user
     *
     * @param userId current user
     * @return current game progress after stand move
     * @throws UserNotFoundException if user will not found
     * @throws GameNotFoundException if game will not found
     */
    private GameResponse standMove(int userId) throws UserNotFoundException, GameNotFoundException {
        double userWinSum = dealerLogic();
        return new GameResponse(
                game.getGameId(),
                userId,
                userWinSum,
                game.getUserCards(),
                game.getDealerCards(),
                game.getStatusGameForUser()
        );
    }

    /**
     * Method for execution dealer logic
     *
     * @return win sum for user after dealers moves
     */
    private double dealerLogic() {
        while (sumPoints(game.getDealerCards()) <= END_DEALER_LOGIC_BOUNDARY) {
            dealCards(1, CardsOwner.DEALER_CARDS);
            sumPoints(game.getDealerCards());
        }

        int currentDealerPoints = sumPoints(game.getDealerCards());
        int currentPlayerPoints = sumPoints(game.getUserCards());

        Winner winner;
        if (currentDealerPoints >= END_DEALER_LOGIC_BOUNDARY && currentDealerPoints <= BLACK_JACK) {
            winner = checkWinner(currentPlayerPoints, currentDealerPoints);
            if (winner == Winner.DEALER) {
                game.setStatusGameForUser(StatusGame.BUST);
            } else if (winner == Winner.DRAW) {
                game.setStatusGameForUser(StatusGame.PUSH);
            } else {
                game.setStatusGameForUser(StatusGame.WIN);
            }
        } else {
            winner = Winner.USER;
            game.setStatusGameForUser(StatusGame.WIN);
        }

        gameDAO.save(game);

        return playerWinSum(currentPlayerPoints, currentDealerPoints, game.getUserBet(), winner, UserStepInGame.NEXT);
    }


    /**
     * method for check permission before hit and stand move
     *
     * @param gameId current game
     * @param userId current user
     * @throws UserNotFoundException if user will not found
     * @throws GameNotFoundException if game will not found
     */
    private void checkMovePermission(String gameId, int userId) throws UserNotFoundException, GameNotFoundException {
        GameProgress currentGame = gameDAO.findOne(gameId);

        if (currentGame == null) {
            throw new GameNotFoundException("Game not found: " + gameId);
        } else {
            this.game = currentGame;
        }
        User currentUser = userDAO.findOne(userId);
        if (currentUser == null) {
            throw new UserNotFoundException("User not found: " + userId);
        }
    }

    /**
     * Method for start new game
     *
     * @param userId current user
     * @param userBet user bet for current game
     * @return current game progress
     * @throws NotEnoughMoneyException if user has no money for this move
     * @throws UserNotFoundException if user will not found
     */
    private GameResponse initGame(int userId, double userBet) throws NotEnoughMoneyException, UserNotFoundException {
        initTable(userId, userBet);
        dealCards(2, CardsOwner.BOTH_CARDS);

        int userPoints = sumPoints(game.getUserCards());
        int dealerPoints = sumPoints(game.getDealerCards());

        Winner winner = checkWinner(userPoints, dealerPoints);
        setGameStatusForUser(userPoints, winner);
        double userWinSum = playerWinSum(userPoints, dealerPoints, userBet, winner, UserStepInGame.FIRST);

        return createResponse(userId, userWinSum, userPoints);
    }

    /**
     * Method for sum user won money
     *
     * @param userPoints current users point
     * @param dealerPoints current dealers point
     * @param userBet current users bet
     * @param winner winner of game
     * @param step first or next step in game
     * @return sum of all money witch user won
     */
    private double playerWinSum(int userPoints, int dealerPoints, double userBet, Winner winner, UserStepInGame step) {
        double winSum;
        if (userPoints == BLACK_JACK) {
            if (winner == Winner.USER && step == UserStepInGame.FIRST){
                winSum = userBet * 2.5;
            } else if (winner == Winner.DRAW){
                winSum = userBet;
            } else {
                winSum = userBet * 2;
            }
        } else if (dealerPoints > BLACK_JACK || winner == Winner.USER) {
            winSum = userBet * 2;
        } else if (winner == Winner.DRAW) {
            winSum = userBet;
        } else {
            winSum = 0.0;
        }
        return winSum;
    }

    /**
     *  Method for definition status game for user
     *
     * @param userPoints current user points
     * @param winner winner of game
     */
    private void setGameStatusForUser(int userPoints, Winner winner) {
        StatusGame statusGameForUser;
        if (userPoints == BLACK_JACK) {
            if (winner == Winner.USER){
                statusGameForUser = StatusGame.WIN;
            } else {
                statusGameForUser = StatusGame.PUSH;
            }
        } else {
            statusGameForUser = StatusGame.CONTINUE;
        }
        game.setStatusGameForUser(statusGameForUser);
        gameDAO.save(game);
    }

    /**
     * Method for creation response on frontend
     *
     * @param userId current user
     * @param winSum user win sum
     * @param userPoints current user points
     * @return response for frontend
     */
    private GameResponse createResponse(int userId, double winSum, int userPoints) {
        List<Card> openDealerCard = new LinkedList<>();
        if (userPoints >= BLACK_JACK) {
            openDealerCard = game.getDealerCards();
        } else {
            openDealerCard.add(game.getDealerCards().get(0));
        }
        return new GameResponse(
                game.getGameId(),
                userId,
                winSum,
                game.getUserCards(),
                openDealerCard,
                game.getStatusGameForUser()
        );
    }

    /**
     * Method for init new table on start
     *
     * @param userId current user
     * @param userBet user bet
     */
    private void initTable(int userId, double userBet) {
        game = new GameProgress();
        game.setUserId(userId);
        game.setUserBet(userBet);
        game.setDeck(deckInitAndShuffle());
        gameDAO.save(game);
    }

    /**
     * Method for dealing cards for user and dealer
     *
     * @param cardsCount cards count for dealing
     * @param cardsOwner cards owner for dealing
     */
    private void dealCards(int cardsCount, CardsOwner cardsOwner) {
        List<Card> playerCards = new LinkedList<>();
        List<Card> dealerCards = new LinkedList<>();
        switch (cardsOwner) {
            case USER_CARDS:
                for(int i = 0; i < cardsCount; i++) {
                    playerCards.add(game.getDeck().getCards().pollFirst());
                }
                break;
            case DEALER_CARDS:
                for(int i = 0; i < cardsCount; i++) {
                    dealerCards.add(game.getDeck().getCards().pollFirst());
                }
                break;
            case BOTH_CARDS:
                for(int i = 0; i < cardsCount; i++) {
                    playerCards.add(game.getDeck().getCards().pollFirst());
                    dealerCards.add(game.getDeck().getCards().pollFirst());
                }
                break;
        }
        game.getUserCards().addAll(playerCards);
        game.getDealerCards().addAll(dealerCards);
        gameDAO.save(game);
    }


    /**
     * Method for save victorious events
     *
     * @param user current user
     * @param response object with current game result
     */
    private void saveVictoriousEvents(User user, GameResponse response) {
        Event winEvent = new Event(user.getId(), Events.win(user.getId(), response.getGameId()), EventType.GAME_TYPE);
        eventDAO.save(winEvent);
        user.setBalance(user.getBalance() + response.getUserWinSum());
        userDAO.save(user);
        Event plusBalance = new Event(user.getId(), Events.updateBalance(user.getId(), response.getUserWinSum()), EventType.BALANCE_TYPE);
        eventDAO.save(plusBalance);
        gameDAO.delete(response.getGameId());
    }

    /**
     * Method for save push events
     *
     * @param user current user
     * @param response object with current game result
     * @param winSum user win sum
     */
    private void savePushEvents(User user, GameResponse response, double winSum) {
        Event pushEvent = new Event(user.getId(), Events.push(user.getId(), response.getGameId()), EventType.GAME_TYPE);
        eventDAO.save(pushEvent);
        user.setBalance(user.getBalance() + winSum);
        userDAO.save(user);
        Event balance = new Event(user.getId(), Events.updateBalance(user.getId(), winSum), EventType.BALANCE_TYPE);
        eventDAO.save(balance);
        gameDAO.delete(response.getGameId());
    }

    /**
     * Method for save continues events
     *
     * @param user current user
     */
    private void saveContinuesEvents(User user) {
        Event continuesEvent = new Event(user.getId(), Events.gameContinues(user.getId(), game.getGameId()), EventType.GAME_TYPE);
        eventDAO.save(continuesEvent);
    }

    /**
     * Method for save bust events
     *
     * @param user current user
     */
    private void saveBustEvents(User user, GameResponse response) {
        Event bustEvent = new Event(user.getId(), Events.bust(user.getId(), response.getGameId()), EventType.GAME_TYPE);
        eventDAO.save(bustEvent);
        gameDAO.delete(response.getGameId());
    }

    /**
     * Method for save user moves events
     *
     * @param event current event
     */
    private void saveMoveEvents(Event event) {
        eventDAO.save(event);
    }

    /**
     * Method for save user moves events
     *
     * @param user current user
     * @param userBet user bet on current game
     */
    private void saveUpdateBalanceEvents(User user, double userBet) {
        user.setBalance(user.getBalance() - userBet);
        userDAO.save(user);
        Event minusBalance = new Event(user.getId(), Events.updateBalance(user.getId(), -userBet), EventType.BALANCE_TYPE);
        eventDAO.save(minusBalance);
    }
}
