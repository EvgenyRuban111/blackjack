package com.ruban.blackjack.service;

import com.ruban.blackjack.model.event.Event;


public interface EventService {

    Event getEventById(Integer eventId);

    void deleteEventById(Integer eventId);

    Iterable<Event> getEventsByUserId(Integer userId);

    void deleteEventsByUserId(Integer userId);

}
