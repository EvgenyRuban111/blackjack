package com.ruban.blackjack.service;

import com.ruban.blackjack.model.User;
import com.ruban.blackjack.exception.UserNotFoundException;

public interface UserService {

    User saveUser(User user);

    User getUserById(Integer userId);

    void deleteUserById(Integer userId);

    boolean updateUserBalance(Integer userId, double balance) throws UserNotFoundException;

}
