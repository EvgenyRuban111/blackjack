package com.ruban.blackjack.dao;

import com.ruban.blackjack.model.game.GameProgress;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GameDAOMongo extends MongoRepository<GameProgress, String> {

}
