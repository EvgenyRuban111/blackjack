package com.ruban.blackjack.dao;

import com.ruban.blackjack.model.User;
import org.springframework.data.repository.CrudRepository;

public interface UserDAO extends CrudRepository<User, Integer> {

}
