package com.ruban.blackjack.dao;

import com.ruban.blackjack.model.event.Event;
import org.springframework.data.repository.CrudRepository;

public interface EventDAO extends CrudRepository<Event, Integer> {

    Iterable<Event> findAllByUserId(int userId);

    void deleteAllByUserId(int userId);

}
