package com.ruban.blackjack.service.implementation;

import com.ruban.blackjack.dao.EventDAO;
import com.ruban.blackjack.model.User;
import com.ruban.blackjack.model.event.Event;
import com.ruban.blackjack.model.event.EventType;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@RunWith(SpringJUnit4ClassRunner.class)
public class EventServiceTest {

    @Mock
    private EventDAO eventDAO;
    private EventServiceImpl eventService;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        eventService = new EventServiceImpl();
        eventService.setEventDAO(this.eventDAO);
    }

    @After
    public void tearDown(){
        verifyNoMoreInteractions(eventDAO);
    }

    @Test
    public void testGetEventByIdSuccess() throws Exception {
        //arrange
        Event event = new Event(10, "balance", EventType.BALANCE_TYPE);
        event.setId(12);
        when(eventDAO.findOne(event.getId())).thenReturn(event);
        //act
        Event result = eventService.getEventById(event.getId());
        //assert
        assertNotNull(result);
        assertSame(result, event);
        verify(eventDAO, times(1)).findOne(event.getId());
    }

    @Test
    public void testGetEventsByUserIdSuccess() throws Exception {
        //arrange
        User userTest = new User("test2", 1000.0);
        userTest.setId(12);
        Event event = new Event(10, "balance", EventType.BALANCE_TYPE);
        List<Event> events = new LinkedList<>();
        events.add(event);
        when(eventDAO.findAllByUserId(userTest.getId())).thenReturn(events);
        //act
        Iterable<Event> result = eventService.getEventsByUserId(userTest.getId());
        //assert
        assertNotNull(result);
        assertTrue(result.iterator().next() == event);
        verify(eventDAO, times(1)).findAllByUserId(userTest.getId());
    }
}
