package com.ruban.blackjack.service.implementation;

import com.ruban.blackjack.dao.EventDAO;
import com.ruban.blackjack.dao.UserDAO;
import com.ruban.blackjack.exception.UserNotFoundException;
import com.ruban.blackjack.model.User;
import com.ruban.blackjack.model.event.Event;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hibernate.validator.internal.util.Contracts.assertNotNull;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
public class UserServiceTest {

    @Mock
    private UserDAO userDAO;
    @Mock
    private EventDAO eventDAO;
    private UserServiceImpl userService;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        userService = new UserServiceImpl();
        userService.setUserDAO(this.userDAO);
        userService.setEventDAO(this.eventDAO);
    }

    @Test
    public void testGetUserByIdSuccess() throws Exception {
        //arrange
        User userTest = new User("test", 100.0);
        userTest.setId(100);
        when(userDAO.findOne(userTest.getId())).thenReturn(userTest);
        //act
        User result = userService.getUserById(userTest.getId());
        //assert
        assertNotNull(result);
        assertSame(result, userTest);
        verify(userDAO, times(1)).findOne(userTest.getId());
        verifyNoMoreInteractions(eventDAO);
    }

    @Test
    public void testSaveUserByIdSuccess() throws Exception {
        //arrange
        User userTest = new User("test", 100.0);
        userTest.setId(100);
        when(userDAO.save(userTest)).thenReturn(userTest);
        //act
        User result = userService.saveUser(userTest);
        //assert
        assertNotNull(result);
        assertSame(result, userTest);
        verify(userDAO, times(1)).save(userTest);
        verifyNoMoreInteractions(eventDAO);
    }

    @Test
    public void testUpdateUserBalanceSuccess() throws Exception {
        //arrange
        User userTest = new User("test", 100.0);
        userTest.setId(100);
        double updateBalance = 100.0;
        when(userDAO.findOne(userTest.getId())).thenReturn(userTest);
        when(userDAO.save(userTest)).thenReturn(userTest);
        //act
        Boolean result = userService.updateUserBalance(userTest.getId(), updateBalance);
        //assert
        assertNotNull(result);
        assertTrue(result);
        verify(userDAO, times(1)).save(userTest);
        verify(userDAO, times(1)).findOne(userTest.getId());
        verify(eventDAO, times(1)).save(isA(Event.class));
        verifyNoMoreInteractions(eventDAO);
    }

    @Test(expected = UserNotFoundException.class)
    public void testUpdateUserNotFoundUserInDB() throws Exception {
        //arrange
        User userTest = new User("test", 100.0);
        userTest.setId(100);
        double updateBalance = 100.0;
        when(userDAO.findOne(userTest.getId())).thenReturn(null);
        when(userDAO.save(userTest)).thenReturn(userTest);
        //act
        Boolean result = userService.updateUserBalance(userTest.getId(), updateBalance);
        //assert
        assertNull(result);
    }

}
