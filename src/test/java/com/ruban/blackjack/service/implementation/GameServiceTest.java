package com.ruban.blackjack.service.implementation;

import com.ruban.blackjack.dao.*;
import com.ruban.blackjack.exception.GameNotFoundException;
import com.ruban.blackjack.exception.NotEnoughMoneyException;
import com.ruban.blackjack.exception.UserNotFoundException;
import com.ruban.blackjack.model.User;
import com.ruban.blackjack.model.event.Event;
import com.ruban.blackjack.model.game.*;
import com.ruban.blackjack.model.response.GameResponse;
import org.junit.*;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import java.util.*;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;

public class GameServiceTest {

    @Mock
    private GameDAOMongo gameDAO;
    @Mock
    private UserDAO userDAO;
    @Mock
    private EventDAO eventDAO;

    private GameServiceImpl gameService;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        gameService = new GameServiceImpl();
        gameService.setUserDAO(this.userDAO);
        gameService.setEventDAO(this.eventDAO);
        gameService.setGameDAO(this.gameDAO);
    }

    @Test
    public void testNewGameSuccess() throws Exception {
        //arrange
        User userTest = new User("test", 100.0);
        userTest.setId(100);
        double userBet = 1.00;
        GameProgress gameProgress = new GameProgress();
        gameProgress.setGameId("1");
        when(userDAO.findOne(userTest.getId())).thenReturn(userTest);
        //act
        GameResponse gameResponse = gameService.startNewGame(userTest.getId(), userBet);
        //assert
        assertTrue(gameResponse != null);
        assertTrue(gameResponse.getDealerCards().size() >= 1);
        assertTrue(gameResponse.getUserCards().size() >= 2);
        verify(userDAO, atLeastOnce()).findOne(userTest.getId());
        verify(userDAO, atLeastOnce()).save(userTest);
        verify(eventDAO, atLeastOnce()).save(isA(Event.class));
        verify(gameDAO, atLeastOnce()).save(isA(GameProgress.class));
        verifyNoMoreInteractions(userDAO, eventDAO);
    }

    @Test(expected = NotEnoughMoneyException.class)
    public void testNewGameNotEnoughMoney() throws Exception {
        //arrange
        User userTest = new User("test", 100.0);
        userTest.setId(100);
        double userBet = 111.00;
        GameProgress gameProgress = new GameProgress();
        gameProgress.setGameId("1");
        when(userDAO.findOne(userTest.getId())).thenReturn(userTest);
        //act
        GameResponse gameResponse = gameService.startNewGame(userTest.getId(), userBet);
        //assert
        assertTrue(gameResponse == null);
    }

    @Test(expected = UserNotFoundException.class)
    public void testNewGameUserNotFound() throws Exception {
        //arrange
        User userTest = new User("test", 100.0);
        userTest.setId(100);
        double userBet = 111.00;
        GameProgress gameProgress = new GameProgress();
        gameProgress.setGameId("1");
        when(userDAO.findOne(userTest.getId())).thenReturn(null);
        //act
        GameResponse gameResponse = gameService.startNewGame(userTest.getId(), userBet);
        //assert
        assertNull(gameResponse);
    }

    @Test
    public void testLeaveGameSuccess() throws Exception {
        //arrange
        User userTest = new User("test", 100.0);
        userTest.setId(100);
        GameProgress game = new GameProgress();
        game.setGameId("12");
        //act
        gameService.leaveGame(game.getGameId(), userTest.getId());
        //assert
        verify(gameDAO, atLeastOnce()).delete(game.getGameId());
        verify(eventDAO, times(1)).save(isA(Event.class));
        verify(gameDAO, times(1)).findOne(game.getGameId());
        verifyNoMoreInteractions(userDAO, eventDAO);
    }

    @Test
    public void testHitMoveSuccess() throws Exception {
        //arrange
        User userTest = new User("test", 100.0);
        userTest.setId(100);
        GameProgress game = new GameProgress();
        game.setGameId("12");
        LinkedList<Card> deck = new LinkedList<>();
        deck.add(new Card(Suit.CLUBS, Rank.ACE));
        deck.add(new Card(Suit.CLUBS, Rank.ACE));
        deck.add(new Card(Suit.CLUBS, Rank.ACE));
        deck.add(new Card(Suit.CLUBS, Rank.ACE));
        game.setDeck(new Deck(deck));
        LinkedList<Card> dealerCards = new LinkedList<>();
        dealerCards.add(new Card(Suit.CLUBS, Rank.ACE));
        dealerCards.add(new Card(Suit.CLUBS, Rank.ACE));
        LinkedList<Card> userCards = new LinkedList<>();
        userCards.add(new Card(Suit.CLUBS, Rank.ACE));
        userCards.add(new Card(Suit.CLUBS, Rank.ACE));
        game.setDealerCards(dealerCards);
        game.setUserCards(userCards);

        when(userDAO.findOne(userTest.getId())).thenReturn(userTest);
        when(gameDAO.findOne(game.getGameId())).thenReturn(game);
        //act
        GameResponse gameResponse = gameService.hit(game.getGameId(), userTest.getId());
        //assert
        assertTrue(gameResponse != null);
        assertTrue(gameResponse.getDealerCards().size() >= 2);
        assertTrue(gameResponse.getUserCards().size() >= 2);
        verify(userDAO, atLeastOnce()).findOne(userTest.getId());
        verify(eventDAO, atLeastOnce()).save(isA(Event.class));
        verify(gameDAO, times(1)).findOne(game.getGameId());
        verifyNoMoreInteractions(userDAO, eventDAO);
    }

    @Test(expected = UserNotFoundException.class)
    public void testHitMoveUserNotFound() throws Exception {
        //arrange
        User userTest = new User("test", 100.0);
        userTest.setId(100);
        GameProgress game = new GameProgress();
        game.setGameId("12");
        when(userDAO.findOne(userTest.getId())).thenReturn(null);

        //act
        GameResponse gameResponse = gameService.hit(game.getGameId(), userTest.getId());

        //assert
        assertNull(gameResponse);
    }

    @Test(expected = GameNotFoundException.class)
    public void testHitMoveGameNotFound() throws Exception {
        //arrange
        User userTest = new User("test", 100.0);
        userTest.setId(100);
        GameProgress game = new GameProgress();
        game.setGameId("12");
        when(userDAO.findOne(userTest.getId())).thenReturn(userTest);
        when(gameDAO.findOne(game.getGameId())).thenReturn(null);

        //act
        GameResponse gameResponse = gameService.hit(game.getGameId(), userTest.getId());

        //assert
        assertNull(gameResponse);
    }

    @Test
    public void testStandMoveSuccess() throws Exception {
        //arrange
        User userTest = new User("test", 100.0);
        userTest.setId(100);
        GameProgress game = new GameProgress();
        game.setGameId("12");
        LinkedList<Card> deck = new LinkedList<>();
        deck.add(new Card(Suit.CLUBS, Rank.ACE));
        deck.add(new Card(Suit.CLUBS, Rank.ACE));
        deck.add(new Card(Suit.CLUBS, Rank.ACE));
        deck.add(new Card(Suit.CLUBS, Rank.ACE));
        game.setDeck(new Deck(deck));
        LinkedList<Card> dealerCards = new LinkedList<>();
        dealerCards.add(new Card(Suit.CLUBS, Rank.ACE));
        dealerCards.add(new Card(Suit.CLUBS, Rank.ACE));
        LinkedList<Card> userCards = new LinkedList<>();
        userCards.add(new Card(Suit.CLUBS, Rank.ACE));
        userCards.add(new Card(Suit.CLUBS, Rank.ACE));
        game.setDealerCards(dealerCards);
        game.setUserCards(userCards);

        when(userDAO.findOne(userTest.getId())).thenReturn(userTest);
        when(gameDAO.findOne(game.getGameId())).thenReturn(game);
        //act
        GameResponse gameResponse = gameService.stand(game.getGameId(), userTest.getId());
        //assert
        assertTrue(gameResponse != null);
        assertTrue(gameResponse.getDealerCards().size() >= 2);
        assertTrue(gameResponse.getUserCards().size() >= 2);
        verify(userDAO, atLeastOnce()).findOne(userTest.getId());
        verify(userDAO, atLeastOnce()).save(isA(User.class));
        verify(eventDAO, atLeastOnce()).save(isA(Event.class));
        verify(gameDAO, times(1)).findOne(game.getGameId());
        verify(gameDAO, atLeastOnce()).delete(game.getGameId());
        verify(gameDAO, atLeastOnce()).save(isA(GameProgress.class));
        verifyNoMoreInteractions(userDAO, eventDAO, gameDAO);
        verifyNoMoreInteractions(userDAO, eventDAO);
    }

    @Test(expected = UserNotFoundException.class)
    public void testStandMoveUserNotFound() throws Exception {
        //arrange
        User userTest = new User("test", 100.0);
        userTest.setId(100);
        GameProgress game = new GameProgress();
        game.setGameId("12");
        when(userDAO.findOne(userTest.getId())).thenReturn(null);

        //act
        GameResponse gameResponse = gameService.stand(game.getGameId(), userTest.getId());

        //assert
        assertNull(gameResponse);
    }

    @Test(expected = GameNotFoundException.class)
    public void testStandMoveGameNotFound() throws Exception {
        //arrange
        User userTest = new User("test", 100.0);
        userTest.setId(100);
        GameProgress game = new GameProgress();
        game.setGameId("12");
        when(userDAO.findOne(userTest.getId())).thenReturn(userTest);
        when(gameDAO.findOne(game.getGameId())).thenReturn(null);

        //act
        GameResponse gameResponse = gameService.stand(game.getGameId(), userTest.getId());

        //assert
        assertNull(gameResponse);
    }

}
