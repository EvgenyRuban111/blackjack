package com.ruban.blackjack.controller;

import com.ruban.blackjack.model.User;
import com.ruban.blackjack.model.event.Event;
import com.ruban.blackjack.model.event.EventType;
import com.ruban.blackjack.service.EventService;

import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@RunWith(SpringJUnit4ClassRunner.class)
public class EventControllerTest {

    @Mock
    private EventService eventService;
    private EventController eventController;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        eventController = new EventController();
        eventController.setEventService(this.eventService);
    }

    @After
    public void tearDown(){
        verifyNoMoreInteractions(eventService);
    }


    @Test
    public void testGetEventByIdSuccess() throws Exception {
        //arrange
        Event event = new Event(10, "balance", EventType.BALANCE_TYPE);
        event.setId(12);
        ResponseEntity<Event> responseEvent = new ResponseEntity<>(event, HttpStatus.OK);
        when(eventService.getEventById(event.getId())).thenReturn(event);
        //act
        ResponseEntity<Event> result = eventController.getEvent(event.getId());
        //assert
        assertEquals(result, responseEvent);
        verify(eventService, times(1)).getEventById(event.getId());
    }

    @Test
    public void testGetEventByIdFailed() throws Exception {
        //arrange
        Event event = new Event(11, "new game", EventType.GAME_TYPE);
        event.setId(113);
        ResponseEntity<Event> responseEvent = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        when(eventService.getEventById(event.getId())).thenReturn(null);
        //act
        ResponseEntity<Event> result = eventController.getEvent(event.getId());
        //assert
        assertEquals(result, responseEvent);
        verify(eventService, times(1)).getEventById(event.getId());
    }

    @Test
    public void testDeleteEventByIdSuccess() throws Exception {
        //arrange
        Event event = new Event(121, "game", EventType.GAME_TYPE);
        event.setId(22);
        ResponseEntity<String> deleteResponse = new ResponseEntity<>("Success", HttpStatus.OK);
        //act
        ResponseEntity<String> result = eventController.deleteEvent(event.getId());
        //assert
        assertEquals(result, deleteResponse);
        verify(eventService, times(1)).deleteEventById(event.getId());
    }

    @Test
    public void testGetEventByUserIdSuccess() throws Exception {
        //arrange
        User userTest = new User("test2", 1000.0);
        userTest.setId(12);
        Event event = new Event(10, "balance", EventType.BALANCE_TYPE);
        List<Event> events = new LinkedList<>();
        events.add(event);
        ResponseEntity<Iterable<Event>> responseEvents = new ResponseEntity<>(events, HttpStatus.OK);
        when(eventService.getEventsByUserId(userTest.getId())).thenReturn(events);
        //act
        ResponseEntity<Iterable<Event>> result = eventController.getAllEventsByUser(userTest.getId());
        //assert
        assertEquals(result, responseEvents);
        verify(eventService, times(1)).getEventsByUserId(userTest.getId());
    }

    @Test
    public void testDeleteEventsByUserIdSuccess() throws Exception {
        //arrange
        User userTest = new User("test2", 1000.0);
        userTest.setId(12);
        ResponseEntity<String> deleteResponse = new ResponseEntity<>("Success", HttpStatus.OK);
        //act
        ResponseEntity<String> result = eventController.deleteAllEventsByUser(userTest.getId());
        //assert
        assertEquals(result, deleteResponse);
        verify(eventService, times(1)).deleteEventsByUserId(userTest.getId());
    }
}
