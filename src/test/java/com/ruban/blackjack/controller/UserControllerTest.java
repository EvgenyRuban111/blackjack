package com.ruban.blackjack.controller;

import com.ruban.blackjack.model.User;
import com.ruban.blackjack.service.UserService;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@RunWith(SpringJUnit4ClassRunner.class)
public class UserControllerTest {

    @Mock
    private UserService userService;
    private UserController userController;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        userController = new UserController();
        userController.setUserService(this.userService);
    }

    @After
    public void tearDown(){
        verifyNoMoreInteractions(userService);
    }

    @Test
    public void testCreateNewUserSuccess() throws Exception {
        //arrange
        User userTest = new User("test", 100.0);
        ResponseEntity<Integer> responseEntity = new ResponseEntity<>(userTest.getId(), HttpStatus.OK);
        when(userService.saveUser(userTest)).thenReturn(userTest);
        //act
        ResponseEntity<Integer> result = userController.createUser(userTest);
        //assert
        assertEquals(result, responseEntity);
        verify(userService, times(1)).saveUser(userTest);
    }

    @Test
    public void testCreateNewUserFailed() throws Exception {
        //arrange
        ResponseEntity<Integer> noContentResponse = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        //act
        ResponseEntity<Integer> result = userController.createUser(null);
        //assert
        assertEquals(result, noContentResponse);
    }

    @Test
    public void testGetUserByIdSuccess() throws Exception {
        //arrange
        User userTest = new User("test", 100.0);
        userTest.setId(100);
        ResponseEntity<User> responseUser = new ResponseEntity<>(userTest, HttpStatus.OK);
        when(userService.getUserById(userTest.getId())).thenReturn(userTest);
        //act
        ResponseEntity<User> result = userController.getUserById(userTest.getId());
        //assert
        assertEquals(result, responseUser);
        verify(userService, times(1)).getUserById(userTest.getId());
    }

    @Test
    public void testGetUserByIdFailed() throws Exception {
        //arrange
        User userTest = new User("test2", 1000.0);
        userTest.setId(12);
        ResponseEntity<User> responseUser = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        when(userService.getUserById(userTest.getId())).thenReturn(null);
        //act
        ResponseEntity<User> result = userController.getUserById(userTest.getId());
        //assert
        assertEquals(result, responseUser);
        verify(userService, times(1)).getUserById(userTest.getId());
    }

    @Test
    public void testDeleteUserByIdSuccess() throws Exception {
        //arrange
        User userTest = new User("test3", 10030.0);
        userTest.setId(2);
        ResponseEntity<String> deleteResponse = new ResponseEntity<>("Success", HttpStatus.OK);
        //act
        ResponseEntity<String> result = userController.deleteUserById(userTest.getId());
        //assert
        assertEquals(result, deleteResponse);
        verify(userService, times(1)).deleteUserById(userTest.getId());
    }

    @Test
    public void testUpdateBalanceForUserSuccess() throws Exception {
        //arrange
        User userTest = new User("test3", 10030.0);
        userTest.setId(2);
        double newBalance = 100.0;
        when(userService.updateUserBalance(userTest.getId(), newBalance)).thenReturn(true);
        ResponseEntity<String> responseBalance = new ResponseEntity<>("Success", HttpStatus.OK);
        //act
        ResponseEntity<String> result = userController.updateUserBalance(userTest.getId(), newBalance);
        //assert
        assertEquals(result, responseBalance);
        verify(userService, times(1)).updateUserBalance(userTest.getId(), newBalance);
    }

    @Test
    public void testUpdateBalanceForUserFailed() throws Exception {
        //arrange
        User userTest = new User("test4", 1030.0);
        userTest.setId(12);
        double newBalance = 110.0;
        when(userService.updateUserBalance(userTest.getId(), newBalance)).thenReturn(false);
        ResponseEntity<String> responseBalance = new ResponseEntity<>("Failed", HttpStatus.NOT_MODIFIED);
        //act
        ResponseEntity<String> result = userController.updateUserBalance(userTest.getId(), newBalance);
        //assert
        assertEquals(result, responseBalance);
        verify(userService, times(1)).updateUserBalance(userTest.getId(), newBalance);
    }

}
