package com.ruban.blackjack.controller;


import com.ruban.blackjack.model.NewGameRequest;
import com.ruban.blackjack.model.User;
import com.ruban.blackjack.model.game.StatusGame;
import com.ruban.blackjack.model.response.GameResponse;
import com.ruban.blackjack.exception.GameNotFoundException;
import com.ruban.blackjack.exception.NotEnoughMoneyException;
import com.ruban.blackjack.exception.UserNotFoundException;
import com.ruban.blackjack.service.GameService;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@RunWith(SpringJUnit4ClassRunner.class)
public class GameControllerTest {

    @Mock
    private GameService gameService;

    private GameController gameController;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        gameController = new GameController();
        gameController.setGameService(this.gameService);
    }

    @After
    public void tearDown(){
        verifyNoMoreInteractions(gameService);
    }

    @Test
    public void testNewGameStartSuccess() throws Exception {
        //arrange
        User userTest = new User("test", 100.0);
        userTest.setId(1);
        NewGameRequest newGameRequest = new NewGameRequest(userTest.getId(), 9.0);
        GameResponse game = new GameResponse("12", userTest.getId(), 1.0, new LinkedList<>(), new LinkedList<>(), StatusGame.WIN);
        ResponseEntity<GameResponse> response = new ResponseEntity<>(game, HttpStatus.OK);
        when(gameService.startNewGame(userTest.getId(), newGameRequest.getUserBet())).thenReturn(game);
        //act
        ResponseEntity<GameResponse> result = gameController.startNewGame(newGameRequest);
        //assert
        assertEquals(result, response);
        verify(gameService, times(1)).startNewGame(userTest.getId(), newGameRequest.getUserBet());
    }

    @Test
    public void testNewGameStartNotEnoughMoney() throws Exception {
        //arrange
        User userTest = new User("test", 100.0);
        userTest.setId(1);
        NewGameRequest newGameRequest = new NewGameRequest(userTest.getId(), 110.0);
        ResponseEntity<GameResponse> response = new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        when(gameService.startNewGame(userTest.getId(), newGameRequest.getUserBet())).thenThrow(new NotEnoughMoneyException("Not enough money"));
        //act
        ResponseEntity<GameResponse> result = gameController.startNewGame(newGameRequest);
        //assert
        assertEquals(result, response);
        verify(gameService, times(1)).startNewGame(userTest.getId(), newGameRequest.getUserBet());
    }

    @Test
    public void testNewGameStartUserNotFound() throws Exception {
        //arrange
        User userTest = new User("test", 100.0);
        userTest.setId(1);
        NewGameRequest newGameRequest = new NewGameRequest(userTest.getId(), 10.0);
        GameResponse game = new GameResponse("12", userTest.getId(), 1.0, new LinkedList<>(), new LinkedList<>(), StatusGame.WIN);
        ResponseEntity<GameResponse> response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        when(gameService.startNewGame(userTest.getId(), newGameRequest.getUserBet())).thenThrow(new UserNotFoundException("User not found"));
        //act
        ResponseEntity<GameResponse> result = gameController.startNewGame(newGameRequest);
        //assert
        assertEquals(result, response);
        verify(gameService, times(1)).startNewGame(userTest.getId(), newGameRequest.getUserBet());
    }

    @Test
    public void testHitMoveSuccess() throws Exception {
        //arrange
        User userTest = new User("test", 100.0);
        userTest.setId(1);
        GameResponse game = new GameResponse("12", userTest.getId(), 1.0, new LinkedList<>(), new LinkedList<>(), StatusGame.WIN);
        ResponseEntity<GameResponse> response = new ResponseEntity<>(game, HttpStatus.OK);
        when(gameService.hit(game.getGameId(), userTest.getId())).thenReturn(game);
        //act
        ResponseEntity<GameResponse> result = gameController.hit(game.getGameId(), userTest.getId());
        //assert
        assertEquals(result, response);
        verify(gameService, times(1)).hit(game.getGameId(), userTest.getId());
    }

    @Test
    public void testHitMoveUserNotFound() throws Exception {
        //arrange
        User userTest = new User("test", 100.0);
        userTest.setId(1);
        GameResponse game = new GameResponse("12", userTest.getId(), 1.0, new LinkedList<>(), new LinkedList<>(), StatusGame.WIN);
        ResponseEntity<GameResponse> response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        when(gameService.hit(game.getGameId(), userTest.getId())).thenThrow(new UserNotFoundException("User not found"));
        //act
        ResponseEntity<GameResponse> result = gameController.hit(game.getGameId(), userTest.getId());
        //assert
        assertEquals(result, response);
        verify(gameService, times(1)).hit(game.getGameId(), userTest.getId());
    }

    @Test
    public void testHitMoveGameNotFound() throws Exception {
        //arrange
        User userTest = new User("test", 100.0);
        userTest.setId(1);
        GameResponse game = new GameResponse("12", userTest.getId(), 1.0, new LinkedList<>(), new LinkedList<>(), StatusGame.WIN);
        ResponseEntity<GameResponse> response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        when(gameService.hit(game.getGameId(), userTest.getId())).thenThrow(new GameNotFoundException("Game not found"));
        //act
        ResponseEntity<GameResponse> result = gameController.hit(game.getGameId(), userTest.getId());
        //assert
        assertEquals(result, response);
        verify(gameService, times(1)).hit(game.getGameId(), userTest.getId());
    }

    @Test
    public void testStandMoveSuccess() throws Exception {
        //arrange
        User userTest = new User("test", 100.0);
        userTest.setId(1);
        GameResponse game = new GameResponse("12", userTest.getId(), 1.0, new LinkedList<>(), new LinkedList<>(), StatusGame.WIN);
        ResponseEntity<GameResponse> response = new ResponseEntity<>(game, HttpStatus.OK);
        when(gameService.stand(game.getGameId(), userTest.getId())).thenReturn(game);
        //act
        ResponseEntity<GameResponse> result = gameController.stand(game.getGameId(), userTest.getId());
        //assert
        assertEquals(result, response);
        verify(gameService, times(1)).stand(game.getGameId(), userTest.getId());
    }

    @Test
    public void testStandMoveUserNotFound() throws Exception {
        //arrange
        User userTest = new User("test", 100.0);
        userTest.setId(1);
        GameResponse game = new GameResponse("12", userTest.getId(), 1.0, new LinkedList<>(), new LinkedList<>(), StatusGame.WIN);
        ResponseEntity<GameResponse> response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        when(gameService.stand(game.getGameId(), userTest.getId())).thenThrow(new UserNotFoundException("User not found"));
        //act
        ResponseEntity<GameResponse> result = gameController.stand(game.getGameId(), userTest.getId());
        //assert
        assertEquals(result, response);
        verify(gameService, times(1)).stand(game.getGameId(), userTest.getId());
    }

    @Test
    public void testStandMoveGameNotFound() throws Exception {
        //arrange
        User userTest = new User("test", 100.0);
        userTest.setId(1);
        GameResponse game = new GameResponse("12", userTest.getId(), 1.0, new LinkedList<>(), new LinkedList<>(), StatusGame.WIN);
        ResponseEntity<GameResponse> response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
        when(gameService.stand(game.getGameId(), userTest.getId())).thenThrow(new GameNotFoundException("Game not found"));
        //act
        ResponseEntity<GameResponse> result = gameController.stand(game.getGameId(), userTest.getId());
        //assert
        assertEquals(result, response);
        verify(gameService, times(1)).stand(game.getGameId(), userTest.getId());
    }

    @Test
    public void testLeaveGameSuccess() throws Exception {
        //arrange
        User userTest = new User("test", 100.0);
        userTest.setId(1);
        GameResponse game = new GameResponse("12", userTest.getId(), 1.0, new LinkedList<>(), new LinkedList<>(), StatusGame.WIN);
        ResponseEntity<String> response = new ResponseEntity<>("Your leave game with id: " + game.getGameId(), HttpStatus.OK);
        when(gameService.leaveGame(game.getGameId(), userTest.getId())).thenReturn(true);
        //act
        ResponseEntity<String> result = gameController.leaveGame(game.getGameId(), userTest.getId());
        //assert
        assertEquals(result, response);
        verify(gameService, times(1)).leaveGame(game.getGameId(), userTest.getId());
    }

    @Test
    public void testLeaveGameFailed() throws Exception {
        //arrange
        User userTest = new User("test", 100.0);
        userTest.setId(1);
        GameResponse game = new GameResponse("12", userTest.getId(), 1.0, new LinkedList<>(), new LinkedList<>(), StatusGame.WIN);
        ResponseEntity<String> response = new ResponseEntity<>("Your didnt leave game with id: " + game.getGameId(), HttpStatus.OK);
        when(gameService.leaveGame(game.getGameId(), userTest.getId())).thenReturn(false);
        //act
        ResponseEntity<String> result = gameController.leaveGame(game.getGameId(), userTest.getId());
        //assert
        assertEquals(result, response);
        verify(gameService, times(1)).leaveGame(game.getGameId(), userTest.getId());
    }

}
